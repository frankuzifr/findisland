﻿using System;


namespace FindIsland
{
    class Program
    {
        private static void Main(string[] args)
        {
            int m, n, result;
            Random rnd = new Random();
            Console.WriteLine("Карта:");
            m = rnd.Next(3, 5);
            n = rnd.Next(3, 5);
            int[,] ch = new int[m + 2 , n + 2]; //создаем массив
            for (int i = 0; i < m + 2; i++) 
            {
                for (int j = 0; j < n + 2; j++)
                {
                    ch[i, j] = 0; //заполняем массив нулями, чтобы в дальнейшем по краям у нас оставалась вода
                }
            }
            for (int i = 1; i <= m; i++)
            {
                for (int j = 1; j <= n; j++)
                {
                    ch[i, j] = rnd.Next(0, 2); //заполняем массив рандомно 0 и 1, не трогая края матрицы
                }
            }
            for (int i = 0; i < m + 2; i++)
            {
                for (int j = 0; j < n + 2; j++)
                {
                    Console.Write(" " + ch[i, j]); //выводим на экран получившуюся матрицу
                }
                Console.WriteLine();
            }
            result = CountIslands(m, n, ch); //выводим на экран кол-во найденных островов
            Console.WriteLine();
            Console.WriteLine("Островов:");
            Console.Write(result);
            Console.WriteLine();
        }

        //функция для подсчета островов
        private static int CountIslands(int r, int f, int[,] cells)
        {
            int count = 0;
            for (int i = 1; i < r + 1; i++)
            {
                for (int j = 1; j < f + 1; j++)
                {
                    if (cells[i, j] == 1)
                    {
                        count++;
                        Count(i, j, cells);
                    }
                }
            }
            return count;
        }

        //функция для определения размера найденного острова и его удаления
        private static void Count(int i, int j, int[,] cells)
        {
            if (cells[i, j] != 1) 
            {
                return;
            }
            cells[i, j] = 0; //если мы попадаемся на сушу, то мы ее превращаем в воду
            //проходимся по соседним ячейкам найденной суши
            Count(i + 1, j, cells);
            Count(i - 1, j, cells);
            Count(i, j + 1, cells);
            Count(i, j - 1, cells);
        }
    }
}
